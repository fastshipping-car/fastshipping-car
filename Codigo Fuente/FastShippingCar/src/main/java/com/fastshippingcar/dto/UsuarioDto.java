package com.fastshippingcar.dto;

import com.fastshippingcar.model.UsuarioModel;

public class UsuarioDto extends UsuarioModel {
	
	public String getNombreCompleto() {
		return String.format("{0} {1}", getNombre(), getApellido());
	}
}
