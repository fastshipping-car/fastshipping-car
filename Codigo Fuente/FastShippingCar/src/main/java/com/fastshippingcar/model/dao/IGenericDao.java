package com.fastshippingcar.model.dao;

import java.util.Collection;

import com.fastshippingcar.model.GenericModel;

public interface IGenericDao<T extends GenericModel<?>> {
	public abstract void create(T dato);

	public abstract void update(String id, T dato);

	public abstract void delete(String id);

	public abstract Collection<T> get();
}
