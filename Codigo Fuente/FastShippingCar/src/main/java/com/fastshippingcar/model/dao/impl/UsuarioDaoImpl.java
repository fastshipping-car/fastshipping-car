package com.fastshippingcar.model.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fastshippingcar.model.UsuarioModel;
import com.fastshippingcar.model.dao.IUsuarioDao;
import com.github.fabiomaffioletti.firebase.repository.DefaultFirebaseRealtimeDatabaseRepository;
import com.github.fabiomaffioletti.firebase.repository.HttpEntityBuilder;
import com.github.fabiomaffioletti.firebase.service.FirebaseApplicationService;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.SessionCookieOptions;
import com.google.firebase.auth.SessionCookieOptions.Builder;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import java.util.concurrent.TimeUnit;

@Repository
public class UsuarioDaoImpl extends DefaultFirebaseRealtimeDatabaseRepository<UsuarioModel, String>
		implements IUsuarioDao {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper firebaseObjectMapper;

	@Autowired
	private FirebaseApplicationService firebaseApplicationService;

	@Override
	public void create(UsuarioModel dato) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(String id, UsuarioModel dato) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<UsuarioModel> get() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserRecord autenticarUsuario(String emailUsario, String contrasenaUsuario) throws FirebaseAuthException {
		/**
		 * curl
		 * 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]' \
		 * -H 'Content-Type: application/json' \ --data-binary
		 * '{"email":"[user@example.com]","password":"[PASSWORD]","returnSecureToken":true}'
		 */
		String urlApi = new StringJoiner("/", "", ".json").add(
				"https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCLcokhVAGopwL5XItQ-UsfGbKkzgeY-hQ")
				.toString();

		HttpEntity<?> httpEntity = HttpEntityBuilder.create(firebaseObjectMapper, firebaseApplicationService).build();
		Object uriVariables = new Object() {
			public final String email = emailUsario;
			public final String contrasena = contrasenaUsuario;
		};

		UserRecord response = restTemplate.exchange(urlApi, HttpMethod.POST, httpEntity, UserRecord.class, uriVariables)
				.getBody();

		FirebaseAuth firebase = FirebaseAuth.getInstance();
		String uid = "some-uid";
		Builder builder = SessionCookieOptions.builder();
		builder.setExpiresIn(TimeUnit.MINUTES.toMillis(5));
		String customToken = FirebaseAuth.getInstance().createSessionCookie(response.getUid(), builder.build());
		//TODO: Setiar token en la session cokkie
		return response;
	}

	@Override
	public UserRecord registrarUsuario(UsuarioModel usuarioModel) throws FirebaseAuthException {
		FirebaseAuth firebase = FirebaseAuth.getInstance();
		FirebaseApp firebaseApp = FirebaseApp.getInstance();

		CreateRequest request = new CreateRequest();
		request.setEmail(usuarioModel.getEmail());
		request.setPassword(usuarioModel.getContrasena());
		return firebase.createUser(request);
	}

	@Override
	public void cerrarSesion(String token) throws FirebaseAuthException {
		FirebaseAuth firebase = FirebaseAuth.getInstance();

		firebase.revokeRefreshTokens(token);
	}

}
