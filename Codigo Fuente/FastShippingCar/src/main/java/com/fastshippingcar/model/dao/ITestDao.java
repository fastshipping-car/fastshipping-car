package com.fastshippingcar.model.dao;

import com.fastshippingcar.model.TestModel;

public interface ITestDao extends IGenericDao<TestModel> {

}
