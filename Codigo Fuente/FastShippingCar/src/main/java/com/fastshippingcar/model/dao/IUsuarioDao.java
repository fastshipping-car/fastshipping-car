package com.fastshippingcar.model.dao;

import com.fastshippingcar.model.UsuarioModel;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

public interface IUsuarioDao extends IGenericDao<UsuarioModel> {
	public UserRecord autenticarUsuario(String email, String contrasena) throws FirebaseAuthException;

	public UserRecord registrarUsuario(UsuarioModel usuarioModel) throws FirebaseAuthException;

	public void cerrarSesion(String token) throws FirebaseAuthException;
}
