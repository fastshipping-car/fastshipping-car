package com.fastshippingcar.model;

import com.github.fabiomaffioletti.firebase.document.FirebaseId;
import com.google.api.client.util.DateTime;

public abstract class GenericModel<T> {
	public T id;
	private String usuarioCreacion;
	private String usuarioActualizacion;
	private DateTime fechaCreacion;
	private DateTime fechaActualizacion;
	private boolean activo;

	public abstract T getId();

	public abstract void setId(T id);

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public DateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(DateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public DateTime getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(DateTime fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
}
