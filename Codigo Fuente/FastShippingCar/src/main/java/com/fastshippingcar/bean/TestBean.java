package com.fastshippingcar.bean;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import javax.faces.view.ViewScoped;

import com.fastshippingcar.service.ITestService;

@ManagedBean
@SessionScoped
public class TestBean implements Serializable {
//	@ManagedProperty("#{springBeanName}")
//    private SpringBeanClass springBeanName;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3865595449326853385L;

	@Autowired
    private ITestService testService;
	
	private String texto;

	@PostConstruct
	public void onInit() {
		texto = "Texto bean";
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public ITestService getTestService() {
		return testService;
	}

	public void setTestService(ITestService testService) {
		this.testService = testService;
	}
}
