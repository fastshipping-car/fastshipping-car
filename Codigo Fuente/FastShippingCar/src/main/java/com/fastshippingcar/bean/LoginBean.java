package com.fastshippingcar.bean;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import javax.faces.view.ViewScoped;

import com.fastshippingcar.service.ITestService;
import com.fastshippingcar.service.IUsuarioService;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {
//	@ManagedProperty("#{springBeanName}")
//    private SpringBeanClass springBeanName;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3865595449326853385L;

	@Autowired
    private IUsuarioService usuarioService;
	
	private String email;
	private String contrasena;
	private String validacion;

	@PostConstruct
	public void onInit() {
	}
	
	public void autenticarUsuario() {
		try {
			if(usuarioService.autenticarUsuario(email, contrasena) == null) {
				validacion = "Usuario/contraseña incorrecta";
			}else {
				//TODO: Navegar a la otra pantalla
			}
		} catch (Exception e) {
			validacion = "Usuario/contraseña incorrecta";
		}
	}

	public IUsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(IUsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getValidacion() {
		return validacion;
	}

	public void setValidacion(String validacion) {
		this.validacion = validacion;
	}

	
}
