package com.fastshippingcar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fastshippingcar.model.TestModel;
import com.fastshippingcar.model.dao.IGenericDao;
import com.fastshippingcar.model.dao.ITestDao;
import com.fastshippingcar.model.dao.impl.TestDaoImpl;
import com.fastshippingcar.service.ITestService;

@Service
public class TestServiceImpl extends GenericServiceImpl<TestModel> implements ITestService {

	
	@Autowired
	public TestServiceImpl(ITestDao testDao) {
		super(testDao);
		// TODO Auto-generated constructor stub
	}


}
