package com.fastshippingcar.service;

import com.fastshippingcar.dto.UsuarioDto;
import com.fastshippingcar.model.UsuarioModel;
import com.google.firebase.auth.UserRecord;

public interface IUsuarioService extends IGenericService<UsuarioModel> {
	UserRecord autenticarUsuario(String email, String contrasena);

	UserRecord registrarUsuario(UsuarioDto usuarioDto);

	void cerrarSesion(String token);
}
