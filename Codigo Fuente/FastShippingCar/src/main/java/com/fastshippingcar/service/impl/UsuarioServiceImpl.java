package com.fastshippingcar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fastshippingcar.dto.UsuarioDto;
import com.fastshippingcar.model.UsuarioModel;
import com.fastshippingcar.model.dao.IUsuarioDao;
import com.fastshippingcar.service.IUsuarioService;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

@Service
public class UsuarioServiceImpl extends GenericServiceImpl<UsuarioModel> implements IUsuarioService {
	
	private final IUsuarioDao _usuarioDao;
	@Autowired
	public UsuarioServiceImpl(IUsuarioDao usuarioDao) {
		super(usuarioDao);
		_usuarioDao = usuarioDao;
	}

	public UserRecord autenticarUsuario(String email, String contrasena) {
		try {
			return _usuarioDao.autenticarUsuario(email, contrasena);
		}catch (FirebaseAuthException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public UserRecord registrarUsuario(UsuarioDto usuarioDto) {
		try {
			return _usuarioDao.registrarUsuario(usuarioDto);
		}catch (FirebaseAuthException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public void cerrarSesion(String token) {
		try {
			_usuarioDao.cerrarSesion(token);
		}catch (FirebaseAuthException e) {
			// TODO: handle exception
		}
	}
}
