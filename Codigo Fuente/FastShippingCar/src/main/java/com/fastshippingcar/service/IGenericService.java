package com.fastshippingcar.service;

import java.util.Collection;

import com.fastshippingcar.model.GenericModel;

public interface IGenericService<T extends GenericModel<?>> {
	public abstract void create(T dato);

	public abstract void update(String id, T dato);

	public abstract void delete(String id);

	public abstract Collection<T> get();
}
