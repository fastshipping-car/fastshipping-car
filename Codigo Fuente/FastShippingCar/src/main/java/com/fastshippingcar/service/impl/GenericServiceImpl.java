package com.fastshippingcar.service.impl;

import java.util.Collection;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fastshippingcar.model.GenericModel;
import com.fastshippingcar.model.dao.IGenericDao;
import com.fastshippingcar.service.IGenericService;
import com.github.fabiomaffioletti.firebase.repository.DefaultFirebaseRealtimeDatabaseRepository;


public class GenericServiceImpl<T extends GenericModel<?>> implements IGenericService<T> {
	private final IGenericDao<T> _genericDao;
	
	public GenericServiceImpl(IGenericDao<T> genericDao) {
		_genericDao = genericDao;
	}
	
	@Override
	public void create(T dato) {
		_genericDao.create(dato);
	}

	@Override
	public void update(String id, T dato) {
		_genericDao.update(id, dato);
	}

	@Override
	public void delete(String id) {
		_genericDao.delete(id);
	}

	@Override
	public Collection<T> get() {
		return _genericDao.get();
	}

}
