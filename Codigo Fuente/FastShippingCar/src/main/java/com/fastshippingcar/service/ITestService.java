package com.fastshippingcar.service;

import com.fastshippingcar.model.TestModel;

public interface ITestService extends IGenericService<TestModel> {

}
