package com.fastshippingcar.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.fastshippingcar.dto.UsuarioDto;
import com.google.firebase.auth.UserRecord;

@SpringBootTest
public class UsuarioServiceTest {
	
	//@InjectMocks
    //private UserController userController;
	
	@Mock
    private IUsuarioService usuarioService;

    @BeforeAll
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
    
	@Test
	void autenticarUsuario() {
		usuarioService.autenticarUsuario("Correo", "1234");
	}
	
	@Test
	void registrarUsuario() {
		UsuarioDto usuarioDto = new UsuarioDto();
		usuarioDto.setNombre("nombre");
		usuarioDto.setApellido("apellido");
		usuarioDto.setDireccion("direccion");
		usuarioDto.setEmail("email");
		usuarioDto.setActivo(true);
		usuarioService.registrarUsuario(usuarioDto);
	}
	
	@Test
	void cerrarSesionUsuario() {
		UserRecord usuario = usuarioService.autenticarUsuario("Correo", "1234");
		usuarioService.cerrarSesion(usuario.getUid());
	}
}
